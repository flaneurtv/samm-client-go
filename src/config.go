package samm

import (
	"github.com/Netflix/go-env"
	"os"
	"io"
)

// Config holds application configuration data
type Config struct {
	ServiceName        string `env:"SERVICE_NAME"`
	ServiceUUID        string `env:"SERVICE_UUID"`
	ServiceHost        string `env:"SERVICE_HOST"`
	NamespaceListener  string `env:"NAMESPACE_LISTENER"`
	NamespacePublisher string `env:"NAMESPACE_PUBLISHER"`
	InputReader 			 io.Reader
	OutputWriter       io.Writer
	ErrorWriter        io.Writer
}

// GetConfig gets config from environment
func getConfig() (*Config, error) {
	cfg := &Config{
		InputReader: os.Stdin,
		OutputWriter: os.Stdout,
		ErrorWriter: os.Stderr,
	}
	_, err := env.UnmarshalFromEnviron(cfg)
	if cfg.ServiceHost == "" {
		cfg.ServiceHost, _ = os.Hostname()
	}
	return cfg, err
}
