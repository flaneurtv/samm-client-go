package samm

import (
	"encoding/json"
)

// ErrorMessage describes error response for SAMM
type ErrorMessage struct {
	LogLevel   string `json:"log_level"`
	LogMessage string `json:"log_message"`
	CreatedAt  string `json:"created_at"`
}

type LogLevel string

const (
	LogLevelDebug     LogLevel = "debug"
	LogLevelInfo      LogLevel = "info"
	LogLevelNotice    LogLevel = "notice"
	LogLevelWarning   LogLevel = "warning"
	LogLevelError     LogLevel = "error"
	LogLevelCritical  LogLevel = "critical"
	LogLevelAlert     LogLevel = "alert"
	LogLevelEmergency LogLevel = "emergency"
)

var levels = []LogLevel{
	LogLevelDebug, LogLevelInfo, LogLevelNotice, LogLevelWarning,
	LogLevelError, LogLevelCritical, LogLevelAlert, LogLevelEmergency,
}

func (c *Client) LogWithLevel(level LogLevel, logMsg string) {
	message := &ErrorMessage{
		LogLevel:   string(level),
		LogMessage: logMsg,
		CreatedAt:  c.Now(),
	}
	b, err := json.Marshal(message)
	if err != nil {
		c.WriteErrf("Can't marshal response object %s", err)
		return
	}
	err = c.BytesErr(b)
	if err != nil {
		c.WriteErrf("Can't write %s", err)
		return
	}
}
