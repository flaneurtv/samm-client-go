package samm

import (
	"fmt"
	"io"
)

// some helpers
func (c *Client) WriteOutf(format string, a ...interface{}) error {
	return c.WriteOut(fmt.Sprintf(format, a...))
}

func (c *Client) WriteErrf(format string, a ...interface{}) error {
	return c.WriteErr(fmt.Sprintf(format, a...))
}

func (c *Client) WriteOut(s string) error {
	var buf []byte
	buf = append(buf, s...)
	return c.fprint(c.Config.OutputWriter, buf)
}

func (c *Client) WriteErr(s string) error {
	var buf []byte
	buf = append(buf, s...)
	return c.fprint(c.Config.ErrorWriter, buf)
}

func (c *Client) BytesOut(buf []byte) error {
	return c.fprint(c.Config.OutputWriter, buf)
}

func (c *Client) BytesErr(buf []byte) error {
	return c.fprint(c.Config.ErrorWriter, buf)
}

// this one is using sync.Mutex mu to assure things are written out line by line
func (c *Client) fprint(w io.Writer, buf []byte) error {
	if len(buf) == 0 || buf[len(buf)-1] != '\n' {
		buf = append(buf, '\n')
	}
	c.mu.Lock()
	defer c.mu.Unlock()
	_, err := w.Write(buf)

	bytesToSend := len(buf)

	for bytesToSend > 0 {
		n, err := w.Write(buf)
		if err != nil {
			return err
		}
		bytesToSend = bytesToSend - n
	}
	return err
}
