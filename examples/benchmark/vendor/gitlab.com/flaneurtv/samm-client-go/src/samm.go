package samm

import (
	"bufio"
	"encoding/json"
	"log"
	"strings"
	"sync"
	"time"
	"reflect"
  "runtime"
)

// TimeFormat describes ISO 8601 time format for samm
const TimeFormat = "2006-01-02T15:04:05.999Z"

// Message describes message that describes command or successful response to SAMM
type Message struct {
	Topic       string           `json:"topic"`
	ServiceUUID string           `json:"service_uuid,omitempty"`
	ServiceName string           `json:"service_name,omitempty"`
	ServiceHost string           `json:"service_host,omitempty"`
	CreatedAt   string           `json:"created_at,omitempty"`
	Payload     *json.RawMessage `json:"payload,omitempty"`
	PayloadType string           `json:"payload_type,omitempty"`
}

type Client struct {
	Config *Config
	mu     *sync.Mutex
	routes map[string]TopicHandler
}

// I: create a new SAMM Client
func NewClient() *Client {
	cfg, err := getConfig()
	if err != nil {
		log.Printf("Client could not be created: %s", err)
		return nil
	}

	routes := make(map[string]TopicHandler)

	return &Client{
		Config: cfg,
		mu:     &sync.Mutex{},
		routes: routes,
	}
}

// II: Register topics to be processed by corresponding handler callback
// function defined in your code. Topic must not contain namespace prefix nor
// should it be prefixed with "/".
func (c *Client) RegisterTopic(topic string, handler TopicHandler) {
	c.routes[topic] = handler
}

// TopicHandler handler for specific topic
type TopicHandler func(c *Client, msg *Message)

// III: You need to start either Listen() or Scan() in order to process incoming
// messages. Be aware that decode will ignore malformed JSON without issuing an
// error.
func (c *Client) Listen() {
	decoder := json.NewDecoder(c.Config.InputReader)
	for decoder.More() {
		var msg Message
		err := decoder.Decode(&msg)
		if err != nil {
			c.WriteErrf("Error while decoding input %s", err)
		} else {
			c.route(&msg)
		}
	}
}

// Scan produces errors for malformed JSON, while decode will simply ignore them
// We keep both here for benchmarking reasons
func (c *Client) Scan() {
	scanner := bufio.NewScanner(c.Config.InputReader)
	for scanner.Scan() {
		var msg Message
		err := json.Unmarshal(scanner.Bytes(), &msg)
		if err != nil {
			c.WriteErrf("Error while decoding input %s", err)
		} else {
			c.route(&msg)
		}
	}
}

// Creates a new Message with all fields set. Individual fields may be altered
// later as needed.
func (c *Client) NewMessage(topic string, payload interface{}, payloadType string) *Message {
	if c.Config.NamespacePublisher != "" {
		topic = c.Config.NamespacePublisher + "/" + topic
	}
	m := &Message{
		Topic: topic,
		ServiceUUID: c.Config.ServiceUUID,
		ServiceName: c.Config.ServiceName,
		ServiceHost: c.Config.ServiceHost,
		CreatedAt: c.Now(),
		PayloadType: payloadType,
	}
	b, err := json.Marshal(payload)
	if err != nil {
		c.WriteErrf("Can't marshal response object %s", err)
	}
	m.Payload = (*json.RawMessage)(&b)
	return m
}

// Writes a *Message object to Stdout
func (c *Client) EmitMessage(msg *Message) {
	line, err := json.Marshal(msg)
	if err != nil {
		c.WriteErrf("Error while decoding input %s", err)
	}
	c.BytesOut(line)
}

// Helper function returning an ISO 8601 JSON time string
func (c *Client) Now() string {
	return time.Now().Format(TimeFormat)
}

func getFunctionName(i interface{}) string {
    return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func (c *Client) route(mp *Message) {
	//handler, ok := c.routes[c.config.NamespaceListener + "/" + msg.Topic]
	t := strings.TrimPrefix(mp.Topic, c.Config.NamespaceListener)
	t = strings.TrimPrefix(t, "/")
	handler, ok := c.routes[t]
	if ok {
		if strings.Contains(strings.ToLower(getFunctionName(handler)), "async") {
			go handler(c, mp)
		} else {
		  handler(c, mp)
	  }
	}
}
